package assist;

import java.util.List;
import java.util.SortedSet;

/**
 * Special class used for indicating actual subdomains while creating rules
 * Created by Piotr Misiak on 2016-04-03.
 */
public class CartesianIndicator {
    public int [] tab;
    private final int [] limits;
    private final int productsLimit;

    public CartesianIndicator(List<List<SortedSet<NumberedString>>> subdomains) {
        tab = new int[subdomains.size()];
        limits = new int [subdomains.size()];
        for (int i=0; i< limits.length; i++) {
            limits[i] = subdomains.get(i).size();
        }
        int tempProduct = 1;
        for (int e : limits)
            tempProduct *= e;
        productsLimit = tempProduct;
    }

    public int getProductsLimit() {
        return productsLimit;
    }
    public void increment() {
        for (int i=tab.length-1; i>=0; i--) {
            tab[i]++;
            if (tab[i] == limits[i]) {
                tab[i] = 0;
            } else
                break; //break if current value in i-th element did not reach its limit
        }
    }

    public String toString() {
        StringBuilder sb  = new StringBuilder();
        sb.append("[");
        for (int i=0; i< tab.length-1; i++)
            sb.append(tab[i]).append(", ");
        sb.append(tab[tab.length-1]).append("]");
        return sb.toString();
    }
}
