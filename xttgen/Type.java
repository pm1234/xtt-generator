package xttgen;
import assist.NumberedString;
import java.util.*;
import exceptions.*;
/**
 * The class that defines kind, structure and domain for further attributes definitions.
 * It's New version of class representing one item of type.
 * @version 1.1
 * @author Piotr Misiak
 */
public final class Type {	
	/**
	 * This field is used by the SQL module while storing/restoring model into/from database.
	 * If not provided, this should be null; 
	 */
	private String id;
	
	/**
	 * This field contains attribute name. This should be the default identifier that
	 * types are referred.
	 */
	private String name;
	
	/**
	 * 
	 */
	private Integer length;
	
	/**
	 * A field describing base of the type. It can be either numeric, or symbolic.
	 * It can take one of the values: "numeric" or "symbolic".
	 */
	private String base;
	
	/**
	 * A field indicating whether domain is ordered or not. 
	 * Every numeric type has ordered domain by default.
	 * It can take one of the values "yes" or "no".
         * @since 1.1
	 */
	private String ordered;
	
	/**
	 * A filed containing longer description of the type
	 */
	private String description;
	
	/**
	 * A field denoting precision for floating point numbers.
	 * This is a different name for scale in HMR language.
	 */
	private Double step;

	/**
	 * A filed that contains a list of all acceptable values for this type. 
	 * For ordered symbolic domains, it is possible to assign order to the values. 
	 * It allows referring to the values using this number instead of symbolic name. 
	 * If a symbolic domain is marked as ordered, and there are no weights assigned 
	 * explicitly to the domain's values, default numbering is assumed that starts 
	 * from 1 and ends on n, where n is the number of elements in the domain.
         * @since 1.1 it's the type of SortedSet (used implementation is TreeSet)
	 */
	private SortedSet <NumberedString> domain;
        /**
         * The field indicating whether the domain of the type is a range domain (true) or explicit domain (false).
         */
        private boolean isRangeDomain;
        /**
         * Reference to the type domain without weights (used for inference). This field is null when the type is
         * numeric-base or unordered symbolic-base.
         */
        private SortedSet<NumberedString> noWeightDomain;
        
	public static final String BASE_NUMERIC = "numeric";
	public static final String BASE_SYMBOLIC = "symbolic";
	public static final String BASE_UNKNOWN ="unknown";
	

	public static final String ORDERED_YES = "yes";
	public static final String ORDERED_NO = "no";
	public static final String ORDERED_UNKNOWN ="unknown";
	
        
	/**
	 * A constructor that is used for SQL Mapping with ORM.
	 */
	Type(){}
	
	private Type(Builder builder) {			
		this.setBase(builder.base);
		this.setDescription(builder.description);
		this.setDomain(builder.domain);
		this.setId(builder.id);
		this.setLength(builder.length);
		this.setName(builder.name);
		this.setOrdered(builder.ordered);
		this.setStep(builder.step);
                this.setNoWeightDomain(builder.noWeightDomain);
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

    /**
     * Overriden toString() method returns the complete definition of the type, 
     * as it's defined by the HMR language.
     * @return string representation of the type
     */
    @Override
    public String toString() {
        StringBuilder typeToString = new StringBuilder();
        typeToString.append("\r\nxtype [\tname : ").append(this.name)
        .append(" ,\r\n").append("\tbase : ").append(this.base)
        .append(" ,\r\n").append("\tdomain : ").append(this.domain.toString());
        if (this.description != null) {
            typeToString.append(" ,\r\n\tdesc : ");
            typeToString.append(this.description);
        }
        if (this.ordered != null) {
            typeToString.append(" ,\r\n\tordered : ");
            typeToString.append(this.ordered);
        }
        /*if (this.step != null) {
            typeToString.append(" ,\r\n\tstep : ");
            typeToString.append(this.step);
        }*/
        //the above was removed in order to adapt generator to current version of HMR grammar
        typeToString.append(" ] .");
        return typeToString.toString();
    }
	
	public void setName(String name) {
		this.name = name;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getOrdered() {
		return ordered;
	}


	public void setOrdered(String ordered) {
		this.ordered = ordered;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getStep() {
		return step;
	}

	public void setStep(Double step) {
		this.step = step;
	}

	public SortedSet <NumberedString> getDomain() {
		return domain;
	}
        public SortedSet<NumberedString> getNoWeightDomain() {
            return noWeightDomain;
        }
	public void setDomain(SortedSet <NumberedString> domain) {
		this.domain = domain;
	}
        public void setNoWeightDomain(SortedSet<NumberedString> noWeightDomain) {
            this.noWeightDomain = noWeightDomain;
        }
	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}
	
    /**
     * Method returning the value of the isRangeDomain field
     * @return true if the value is true (range domain), false otherwise (explicit domain)
     */
    public boolean isRangeDomain() {
            return isRangeDomain;
        }
    /**
     * Nested class used for indirect creating new type
     */
    public static class Builder{
		private String id;
		private String name;
		private Integer length;
		private String base;
		private String ordered;
		private String description;
		private Double step;
		private SortedSet <NumberedString> domain;
                private SortedSet<NumberedString> noWeightDomain;
                private boolean isRangeDomain = false;
		private String debugInfo;
		
		public Type build() throws BuilderException{
                    if(name == null || base == null || domain == null)
			throw new BuilderException(String.format("Error while building type "+this.name+". The name, base or domain attribute has not been set.\n%s", this.debugInfo));
                    else
			return new Type(this);
                }

		public Builder setId(String id) {
			this.id = id;
			return this;
		}
		
		public Builder setName(String name) {
			this.name = name;
			return this;
		}

		public Builder setBase(String base) {
			this.base = base;
			return this;
		}

		public Builder setOrdered(String ordered) {
			this.ordered = ordered;
			return this;
		}


		public Builder setDescription(String description) {
			this.description = description;
			return this;
		}


		public Builder setStep(Double step) {
			this.step = step;
			return this;
		}


		public Builder setDomain(SortedSet <NumberedString> domain) {
			this.domain = domain;
			return this;
		}
                public Builder setNoWeightDomain(SortedSet <NumberedString> noWeightDomain) {
                    if (!noWeightDomain.isEmpty())
                        this.noWeightDomain = noWeightDomain;
                    else
                        this.noWeightDomain = this.domain;
                    return this;
		}
                
            /**
             * Method setting the isRangeDomain field
             * @param opt option whether the field is to be true (range domain) or false (explicit domain)
             * @return current Builder object
             */
            public Builder setIsRangeDomain (boolean opt) {
                    this.isRangeDomain = opt;
                    return this;
                }

		public Builder setLength(Integer length) {
			this.length = length;
			return this;
		}
		public Builder setDebugInfo(String debugInfo) {
			this.debugInfo = debugInfo;
			return this;
		}
                
		public String getDebugInfo() {
                    return this.debugInfo;
                }
	}
	

}